package com.example.latitudetracking;

import java.util.Date;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView tvEnabledGPS;
    TextView tvStatusGPS;
    TextView tvLocationGPS;
    TextView tvEnabledNet;
    TextView tvStatusNet;
    TextView tvLocationNet;

    TextView results;
    Button buttonLocationSettings;
    Button buttonStart;
    Button buttonShop;
    Button map;

    private LocationManager locationManager;
    StringBuilder sbGPS = new StringBuilder();
    StringBuilder sbNet = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvEnabledGPS = findViewById(R.id.tvEnabledGPS);
        tvStatusGPS = findViewById(R.id.tvStatusGPS);
        tvLocationGPS = findViewById(R.id.tvLocationGPS);
        tvEnabledNet = findViewById(R.id.tvEnabledNet);
        tvStatusNet = findViewById(R.id.tvStatusNet);
        tvLocationNet = findViewById(R.id.tvLocationNet);
        results = findViewById(R.id.tv_results);
        buttonLocationSettings = findViewById(R.id.btnLocationSettings);
        buttonStart = findViewById(R.id.b_start);
        buttonShop = findViewById(R.id.b_stop);
        map = findViewById(R.id.map);
        buttonLocationSettings.setOnClickListener(this);
        buttonStart.setOnClickListener(this);
        buttonShop.setOnClickListener(this);
        map.setOnClickListener(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {

        //  В onPause отключаем слушателя методом removeUpdates.
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled();
        }

        @Override
        public void onProviderEnabled(String provider) {
            checkEnabled();

            if ((ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) &&
                    (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED)) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            showLocation(locationManager.getLastKnownLocation(provider));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                tvStatusGPS.setText("Status: " + String.valueOf(status));
            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                tvStatusNet.setText("Status: " + String.valueOf(status));
            }
        }
    };

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            tvLocationGPS.setText(formatLocation(location));
            //Toast.makeText(this, "UserXXX находится:" + formatLocation(location) , Toast.LENGTH_SHORT).show();
            results.append(formatLocation(location)+"\n");
        } else if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
            tvLocationNet.setText(formatLocation(location));
           // Toast.makeText(this, "XXXUser находится:" + formatLocation(location) , Toast.LENGTH_SHORT).show();
            results.append(formatLocation(location)+"\n");
        }
    }


    private String formatLocation(Location location) {
        if (location == null)
            return "";

        return String.format(
                "Coordinates: lat = %1$.4f, lon = %2$.4f, time = %3$tF %3$tT",
                location.getLatitude(), location.getLongitude(), new Date(
                        location.getTime()));
    }

    private void checkEnabled() {
        tvEnabledGPS.setText("Enabled: "
                + locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER));
        tvEnabledNet.setText("Enabled: "
                + locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLocationSettings:
                // открыть вкладку настоек
                startActivity(new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                break;
            case R.id.b_start:

/*                В onResume вешаем слушателя с помощью метода requestLocationUpdates. На вход передаем:

            - тип провайдера: GPS_PROVIDER или NETWORK_PROVIDER
                    - минимальное время (в миллисекундах) между получением данных. Я укажу здесь 10
                    секунд, мне этого вполне хватит. Если хотите получать координаты без задержек –
                    передавайте 0. Но учитывайте, что это только минимальное время.
                    Реальное  ожидание может быть дольше.
                    - минимальное расстояние (в метрах). Т.е. если ваше местоположение изменилось
                    на указанное кол-во метров, то вам придут новые координаты.
                - слушатель, объект locationListener, который рассмотрим ниже

                Также здесь обновляем на экране инфу о включенности провайдеров.*/
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Toast toast = Toast.makeText(this, "Permission failed", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        1000 * 10,
                        10,
                        locationListener);

                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        1000 * 10,
                        10,
                        locationListener);
                checkEnabled();


                break;
            case R.id.b_stop:
                // отключаем слушателя методом removeUpdates.
                locationManager.removeUpdates(locationListener);
                tvEnabledNet.setText("Enabled: false");
                tvEnabledGPS.setText("Enabled: false");

                tvLocationGPS.setText("");
                tvLocationNet.setText("");
                break;
            case R.id.map:
                Intent intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
                break;
        }


    }
}

